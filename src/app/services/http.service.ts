import { Injectable } from '@angular/core';
import { delay, of } from 'rxjs';
import { Names } from './MockData';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  get(url:string){
    const duration = Math.floor(Math.random()*3000)
    return of(Names).pipe(delay(duration));
  }

  constructor() { }
}
