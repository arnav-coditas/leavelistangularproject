import { IData} from "./IData";
export const Names  :IData[] = [
    {festivals:"Holi", date:"March 3", isSelected:false },
    {festivals:"Diwali", date:"October 21", isSelected:false},
    {festivals:"Christmas", date:"December 25", isSelected:false},
    {festivals:"Ganesh Chaturthi", date:"September 19", isSelected:false},
    {festivals:"Gokulashtami", date:"August 30", isSelected:false},
    {festivals:"Ram Nanmi", date:"April 10", isSelected:false},
    {festivals:"Navratri", date:"October 15", isSelected:false},
    {festivals:"Sankrant", date:"January 14", isSelected:false},
    {festivals:"Maha Shivratri", date:"February 18", isSelected:false},
    {festivals:"Gudi Padwa", date:"March 22", isSelected:false}
];