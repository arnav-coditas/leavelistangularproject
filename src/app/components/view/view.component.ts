import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { IData } from 'src/app/services/IData';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  logs :any;
  constructor(private data:DataService ){}
    buttonTriggered(e:any) {
      console.log(e);
      
    }
  ngOnInit(): void {
    this.getLogs()
  }
    buttonTriggered2(e:any) {
     
    }
  
    getLogs() {
     this.data.getAllData().subscribe((response)=>
     this.logs=response);

    }
}
